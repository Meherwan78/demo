module "user1webserver" {
    source = "github.com/mounishareddy574/webserver.git"
    region = "us-east-2"
    key_name = "aws-ohio"
    ami      = "ami-03f38e546e3dc59e1"
    instance_type = var.instance_type
    private_key_path = var.private_key_path
  
}
